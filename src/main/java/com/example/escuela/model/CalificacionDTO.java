/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.escuela.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 *
 * @author josesebastiandominguezmartinez
 */
public class CalificacionDTO {
    private float calificacion;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date fecha_registro;
    private int id_t_materias;
    private int id_t_usuarios;

    // Incluye getters y setters

    public float getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(float calificacion) {
        this.calificacion = calificacion;
    }

    public Date getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Date fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public int getId_t_materias() {
        return id_t_materias;
    }

    public void setId_t_materias(int id_t_materias) {
        this.id_t_materias = id_t_materias;
    }

    public int getId_t_usuarios() {
        return id_t_usuarios;
    }

    public void setId_t_usuarios(int id_t_usuarios) {
        this.id_t_usuarios = id_t_usuarios;
    }

}
