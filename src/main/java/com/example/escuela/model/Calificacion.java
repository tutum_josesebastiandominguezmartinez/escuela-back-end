/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.escuela.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author josesebastiandominguezmartinez
 */
@Entity
@Table(name = "t_calificaciones")
public class Calificacion {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id_t_calificaciones;
    private float calificacion;
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd/MM/yyyy")
    @Column(name = "fecha_registro", columnDefinition = "TIMESTAMP WITH TIME ZONE")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date fecha_registro;
    @ManyToOne
    @JoinColumn(name = "id_t_materias")
    private Materia materia;
    @ManyToOne
    @JoinColumn(name = "id_t_usuarios")
    private Alumno alumno;

    public Calificacion() {
    }

    public Calificacion(float calificacion, Date fecha_registro, Materia materia, Alumno alumno) {
        this.calificacion = calificacion;
        this.fecha_registro = fecha_registro;
        this.materia = materia;
        this.alumno = alumno;
    }

    public Long getId_t_calificaciones() {
        return id_t_calificaciones;
    }

    public void setId_t_calificaciones(Long id_t_calificaciones) {
        this.id_t_calificaciones = id_t_calificaciones;
    }

    public float getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(float calificacion) {
        this.calificacion = calificacion;
    }

    public Date getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Date fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public Materia getMateria() {
        return materia;
    }

    public void setMateria(Materia materia) {
        this.materia = materia;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }
}
