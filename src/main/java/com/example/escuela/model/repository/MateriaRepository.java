/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.escuela.model.repository;

import com.example.escuela.model.Materia;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author josesebastiandominguezmartinez
 */
public interface MateriaRepository extends JpaRepository<Materia , Long>{
    
}
