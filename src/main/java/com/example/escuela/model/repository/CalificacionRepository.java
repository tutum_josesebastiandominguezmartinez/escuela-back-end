/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.escuela.model.repository;

import com.example.escuela.model.Calificacion;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author josesebastiandominguezmartinez
 */
@Repository
public interface CalificacionRepository extends JpaRepository<Calificacion, Long>{
    @Query("SELECT c FROM Calificacion c WHERE c.alumno.id_t_usuarios = :id_t_usuarios order by c.materia.id_t_materias")
    List<Calificacion> findByAlumno_Id_t_usuarios(@Param("id_t_usuarios") int id_t_usuarios);
}