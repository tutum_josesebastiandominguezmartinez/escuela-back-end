/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.escuela.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 *
 * @author josesebastiandominguezmartinez
 */
public class Calificaciones {
    private Long id_t_usuario;
    private String nombre;
    private String apellido;
    private Long id_t_calificaciones;
    private float calificacion;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date fecha_registro;
    private Materia materia;
    private Alumno alumno;
}
