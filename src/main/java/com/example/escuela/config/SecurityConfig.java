/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.escuela.config;

import com.example.escuela.filter.ApiKeyFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import java.util.Arrays;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.cors.CorsConfiguration;

/**
 *
 * @author josesebastiandominguezmartinez
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .cors() // Habilitar la configuración de CORS
                .and()
                .authorizeRequests()
                .antMatchers("/**").permitAll() // Rutas públicas sin autenticación
                .anyRequest().authenticated()
                .and()
                .addFilterBefore(apiKeyFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    public ApiKeyFilter apiKeyFilter() throws Exception {
        return new ApiKeyFilter();
    }
    
    @Bean
    public UrlBasedCorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("*")); // Permitir todas las solicitudes de cualquier origen
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE")); // Permitir métodos HTTP permitidos
        configuration.setAllowedHeaders(Arrays.asList("*")); // Permitir todas las cabeceras
        configuration.setMaxAge(3600L); // Tiempo máximo de almacenamiento en caché de las respuestas preflight

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration); // Aplicar la configuración a todas las rutas
        return source;
    }
}
