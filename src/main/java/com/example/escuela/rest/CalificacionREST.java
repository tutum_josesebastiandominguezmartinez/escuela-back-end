/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.escuela.rest;

import com.example.escuela.model.Alumno;
import com.example.escuela.model.Request;
import com.example.escuela.model.Calificacion;
import com.example.escuela.model.Materia;
import com.example.escuela.model.CalificacionDTO;
import com.example.escuela.service.CalificacionService;
import java.net.URI;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author josesebastiandominguezmartinez
 */
@RestController
@RequestMapping("/api/calificacion")
public class CalificacionREST {

    @Autowired
    private CalificacionService calificacionService;

    @GetMapping("{id_t_usuarios}")
    private ResponseEntity<Object> getAllCalificacionesByAlumno(@PathVariable("id_t_usuarios") int id_t_usuarios) {
        List<Map<String, Object>> resultList = new ArrayList<>();
        float promedio = 0;
        List<Calificacion> lista = calificacionService.findByAlumno_Id_t_usuarios(id_t_usuarios);
        for (Calificacion calificacion : lista) {
            // Crear objetos de datos
            Map<String, Object> data = new HashMap<>();
            data.put("id_t_usuarios", calificacion.getAlumno().getId_t_usuarios());
            data.put("nombre", calificacion.getAlumno().getNombre());
            data.put("apellido", calificacion.getAlumno().getAp_paterno());
            data.put("materia", calificacion.getMateria().getNombre());
            data.put("calificacion", calificacion.getCalificacion());
            // En esta linea de código estamos indicando el nuevo formato que queremos para nuestra fecha.
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            // Aqui usamos la instancia formatter para darle el formato a la fecha. Es importante ver que el resultado es un string.
            String fechaTexto = formatter.format(calificacion.getFecha_registro());
            data.put("fecha_registro", fechaTexto);
            data.put("id_t_calificaciones", calificacion.getId_t_calificaciones());
            data.put("id_t_materias", calificacion.getMateria().getId_t_materias());
            // Agregar objetos de datos a la lista de resultados
            resultList.add(data);
            // Se suma la calificación
            promedio += calificacion.getCalificacion();
        }
        // Calcular el promedio de las calificaciones
        if(!lista.isEmpty()){
            promedio /= lista.size();
        }
        // Agregar el objeto de promedio a la lista de resultados
        Map<String, Object> promedioObj = new HashMap<>();
        DecimalFormat df = new DecimalFormat("#.#");
        promedioObj.put("promedio", df.format(promedio));
        resultList.add(promedioObj);
        return ResponseEntity.ok(resultList);
    }

    @PostMapping
    private ResponseEntity<Request> saveCalificacion(@RequestBody CalificacionDTO calificacionDTO) {
        try {
            Calificacion calificacion = new Calificacion();
            calificacion.setCalificacion(calificacionDTO.getCalificacion());
            /*
            // En esta linea de código estamos indicando el nuevo formato que queremos para nuestra fecha.
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            // Aqui usamos la instancia formatter para darle el formato a la fecha. Es importante ver que el resultado es un string.
            String fechaTexto = formatter.format(calificacionDTO.getFecha_registro());*/
            calificacion.setFecha_registro(calificacionDTO.getFecha_registro());
            Materia materia = new Materia();
            materia.setId_t_materias(calificacionDTO.getId_t_materias());
            calificacion.setMateria(materia);
            Alumno alumno = new Alumno();
            alumno.setId_t_usuarios(calificacionDTO.getId_t_usuarios());
            calificacion.setAlumno(alumno);
            Calificacion calificacionSave = calificacionService.save(calificacion);
            return ResponseEntity.created(new URI("/calificaciones/" + calificacionSave.getId_t_calificaciones())).body(new Request("Ok", "Calificación registrada"));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @PutMapping("{id_t_calificaciones}")
    public ResponseEntity<Request> updateCalificacion(@PathVariable int id_t_calificaciones, @RequestBody CalificacionDTO calificacionDetailsDTO) {
        Calificacion updateCalificacion = calificacionService.findById(Long.valueOf(id_t_calificaciones))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Calificacion not found with id: " + id_t_calificaciones));
        try {
            // Actualizar los detalles de la calificación
            updateCalificacion.setCalificacion(calificacionDetailsDTO.getCalificacion());
            updateCalificacion.setFecha_registro(calificacionDetailsDTO.getFecha_registro());
            Materia materia = new Materia();
            materia.setId_t_materias(calificacionDetailsDTO.getId_t_materias());
            updateCalificacion.setMateria(materia);
            Alumno alumno = new Alumno();
            alumno.setId_t_usuarios(calificacionDetailsDTO.getId_t_usuarios());
            updateCalificacion.setAlumno(alumno);
            // Guardar la calificación actualizada
            calificacionService.save(updateCalificacion);
            return ResponseEntity.ok(new Request("Ok", "Calificación actualizada"));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @DeleteMapping("{id_t_calificaciones}")
    public ResponseEntity<Request> deleteCalificacion(@PathVariable("id_t_calificaciones") int id_t_calificaciones) {
        Long id = Long.valueOf(id_t_calificaciones);
        if (calificacionService.existsById(id)) {
            calificacionService.deleteById(id);
            return ResponseEntity.ok(new Request("Ok", "Calificación eliminada"));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
}
