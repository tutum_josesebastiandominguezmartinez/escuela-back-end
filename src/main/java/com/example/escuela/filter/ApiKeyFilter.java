/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.escuela.filter;

import org.springframework.web.filter.OncePerRequestFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Value;

/**
 *
 * @author josesebastiandominguezmartinez
 */
public class ApiKeyFilter extends OncePerRequestFilter {

    private static final String API_KEY_HEADER = "Api-Key";

    private String REQUIRED_API_KEY;

    @Value("${spring.security.user.password}")
    public void setRequiredApiKey(String requiredApiKey) {
        this.REQUIRED_API_KEY = requiredApiKey;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String apiKey = request.getHeader(API_KEY_HEADER);

        if (apiKey != null && apiKey.equals(REQUIRED_API_KEY)) {
            // API Key válida, permite la solicitud
            filterChain.doFilter(request, response);
        } else {
            // API Key no válida, devuelve un error de acceso no autorizado
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
}
